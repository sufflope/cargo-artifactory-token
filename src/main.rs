#![doc = concat!(nickname!(), " — ", env!("CARGO_PKG_DESCRIPTION"))]
//!
#![doc = concat!("![Screen recording of a token creation](", env!("CARGO_PKG_REPOSITORY"), "/-/raw/main/docs/recording.webm){width=865}")]
//!
//! # Installation
//!
//! ```shell
#![doc = concat!("cargo install --locked ", env!("CARGO_PKG_NAME"))]
//! ```
//!
//! # Usage
//!
//! See help for a detailed description of available commands, their arguments, and their default values:
//!
//! ```shell
#![doc = command!("--help")]
//! ```
//!
//! ## Arguments
//!
//! Each argument `some-argument` can be set with a short (`-s`) or long (`--some-argument`) CLI argument, or read from an upper snake case environment variable (`SOME_ARGUMENT`).
//!
//! ### Source
//!
//! - **endpoint**: URL of the Artifactory token creation endpoint
//! - **username**: your Artifactory username (e.g. `flastname` for First Lastname)
//! - **duration**: desired token duration
//!
//! ### Credentials
//!
//! - **file**: path to your cargo credentials file
//! - **registry**: name of the registry in your credentials file
//!
//! ### Token
//!
//! - **access-token**: your private token value
//! - **token-type**: your private token type
//!
//! ### Global arguments
//!
//! These arguments apply to all commands.
//!
//! - **quiet**: suppress all superfluous output
//!
//! ## Commands
//!
//! ### Create a token
//!
//! Retrieve a token and save it with its renewal information. This is the default subcommand.
//!
//! Takes [Source](#source) and [Credentials](#credentials) arguments.
//!
//! ```shell
#![doc = command!("create")]
//! ```
//!
//! ### Generate a token
//!
//! Retrieve a token and display it.
//!
//! Takes [Source](#source) arguments.
//!
//! ```shell
#![doc = command!("generate")]
//! ```
//!
//! ### Renew a token
//!
//! Retrieve a token using saved renewal information and save it.
//!
//! Takes [Credentials](#credentials) arguments.
//!
//! ```shell
#![doc = command!("renew")]
//! ```
//!
//! ### Save an existing token
//!
//! Save a token retrieved by another means.
//!
//! Takes [Credentials](#credentials) and [Token](#token) arguments.
//!
//! ```shell
#![doc = command!("save")]
//! ```

macro_rules! name {
    () => {
        "artifactory-token"
    };
}

macro_rules! nickname {
    () => {
        "\u{1F980}-\u{1F438}-\u{1FA99}"
    };
}

macro_rules! command {
    ($command:literal) => {
        command!(concat!(" ", $command))
    };
    ($command:expr) => {
        concat!("cargo ", name!(), $command)
    };
    () => {
        command!(concat!(""))
    };
}

mod artifactory;
mod credentials;
mod home;
mod log;
mod password;
mod source;

use std::{fmt, io};

use clap::{Args, Parser, Subcommand};
use serde::Deserialize;
#[cfg(test)]
use serde::Serialize;

use crate::{
    credentials::{Credentials, File},
    home::{Home, System},
    log::Log,
    password::{Password, Prompt},
    source::Source,
};

const NAME: &str = name!();
const REGISTRY: &str = "artifactory";

/// Retrieve access tokens from Artifactory and save them in your cargo credentials file.
///
/// Subcommand is optional and defaults to `create`.
#[derive(Parser)]
#[cfg_attr(test, derive(Debug))]
#[command(version, long_about, help_expected = true)]
#[clap(infer_subcommands = true, subcommand_negates_reqs = true)]
struct Opts<Default>
where
    Default: Home,
{
    /// Optional first positional argument to allow running as cargo subcommand
    #[clap(hide = true, value_parser = clap::builder::PossibleValuesParser::new([NAME]))]
    unused: Option<String>,

    /// Parse `Create` args as fallback command if none was explicitly provided
    #[clap(flatten)]
    create: Create<Default>,

    /// Suppress all superfluous output
    #[clap(global = true, short, long, env)]
    quiet: bool,

    #[clap(subcommand)]
    command: Option<Command<Default>>,
}

impl<Default> From<Opts<Default>> for Command<Default>
where
    Default: Home,
{
    fn from(opts: Opts<Default>) -> Self {
        opts.command.unwrap_or(Self::Create(opts.create))
    }
}

#[derive(Subcommand)]
#[cfg_attr(test, derive(Debug))]
enum Command<Default>
where
    Default: Home,
{
    /// Retrieve a token and save it with its renewal information
    Create(#[clap(flatten)] Create<Default>),

    /// Retrieve a token and display it
    Generate(#[clap(flatten)] Source),

    /// Retrieve a token using saved renewal information and save it
    Renew(#[clap(flatten)] Credentials<File<Default>>),

    /// Save an existing token
    Save {
        #[clap(flatten)]
        credentials: Credentials<File<Default>>,

        #[clap(flatten)]
        token: Token,
    },
}

impl<Default> fmt::Display for Command<Default>
where
    Default: Home,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let id = match self {
            Self::Create(_) => "create",
            Self::Generate(_) => "generate",
            Self::Renew(_) => "renew",
            Self::Save { .. } => "save",
        };
        let about = Self::augment_subcommands(clap::Command::default())
            .find_subcommand(id)
            .and_then(|c| c.get_about())
            .unwrap()
            .to_string()
            .to_lowercase();
        write!(f, "{about}")
    }
}

#[derive(Args)]
#[cfg_attr(test, derive(Debug))]
struct Create<Default>
where
    Default: Home,
{
    #[clap(flatten)]
    source: Source,

    #[clap(flatten)]
    credentials: Credentials<File<Default>>,
}

#[derive(Args, Deserialize)]
#[cfg_attr(test, derive(Debug, PartialEq, Serialize))]
pub struct Token {
    /// Your private token value
    #[clap(short, long, env)]
    access_token: String,

    /// Your private token type
    #[clap(short, long, env, default_value = "Bearer")]
    token_type: String,
}

impl fmt::Display for Token {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{} {}", self.token_type, self.access_token)
    }
}

#[tokio::main]
async fn main() {
    let opts = Opts::try_parse();
    let log = Log::from(opts.as_ref().map(|opts| opts.quiet).unwrap_or_default());
    run::<System, Prompt, _, _>(opts, log).await
}

async fn run<Default, In, Out, Err>(
    opts: Result<Opts<Default>, clap::Error>,
    mut log: Log<Out, Err>,
) where
    Default: Home,
    In: Password,
    Out: io::Write,
    Err: io::Write,
{
    let result = async {
        let command = Command::from(opts?);

        log.log(format!(
            "Welcome to <bold>{}</>! <details>{} will {command}</>",
            env!("CARGO_PKG_NAME"),
            nickname!()
        ))
        .newline();

        let (token, credentials, source) = match command {
            Command::Create(Create {
                source,
                credentials,
            }) => (
                source.token::<In, _, _>(&mut log).await?,
                Some(credentials.validate()?),
                Some(source),
            ),
            Command::Generate(source) => (source.token::<In, _, _>(&mut log).await?, None, None),
            Command::Renew(credentials) => {
                let credentials = credentials.validate()?;
                (
                    credentials
                        .renewal(&mut log)?
                        .token::<In, _, _>(&mut log)
                        .await?,
                    Some(credentials),
                    None,
                )
            }
            Command::Save { credentials, token } => (token, Some(credentials.validate()?), None),
        };

        if let Some(credentials) = credentials {
            credentials.save(&mut log, token, source.as_ref())?;
        } else {
            log.log("\u{1F4CB}<bold>Access token</>:\n\n<dimmed>\u{2700} ---</>")
                .out(token)?
                .log("<dimmed>-----</>")
                .newline();
        }

        anyhow::Ok(())
    }
    .await;

    log.finish(result)
}

#[cfg(test)]
mod tests {
    use anyhow::anyhow;
    use e2p_fileflags::{FileFlags, Flags};
    use fs_err as fs;
    use is_root::is_root;
    use reqwest::StatusCode;
    use tempfile::NamedTempFile;

    use super::*;
    use crate::{
        artifactory::tests::{duration, Artifactory},
        credentials::{Registry, RenewError, SaveError, REGISTRIES},
        home::tests::Wanderer,
        log::tests::{Buffer, Output},
        password::tests::Hardcoded,
    };

    pub const USERNAME: &str = "me-myself-i";

    impl Token {
        pub fn test() -> Self {
            Self {
                access_token: "bar".to_string(),
                token_type: "foo".to_string(),
            }
        }
    }

    #[tokio::test]
    async fn create() {
        for subcommand in [Some("create"), Some("c"), None] {
            for (username, result) in [
                (USERNAME, Ok(())),
                (
                    Wanderer::NAME,
                    Err(source::Error::StatusCode(StatusCode::UNAUTHORIZED)),
                ),
            ] {
                let artifactory = Artifactory::default();
                let duration = duration().to_string();
                let file = NamedTempFile::new().unwrap();
                let path = file.path().display().to_string();
                let url = artifactory.url();

                let mut opts = vec![
                    NAME,
                    "-e",
                    url.as_str(),
                    "-u",
                    username,
                    "-d",
                    &duration,
                    "-f",
                    &path,
                    "-r",
                    REGISTRY,
                ];

                if let Some(subcommand) = subcommand {
                    opts.insert(1, subcommand);
                }

                let mut out = Buffer::out();
                let mut err = Buffer::err();

                run::<System, Hardcoded, _, _>(
                    Opts::try_parse_from(opts),
                    Log::boring(&mut out, &mut err),
                )
                .await;

                out.assert_empty();

                match result {
                    Ok(()) => {
                        err.assert_empty();
                        let registry = toml_edit::ser::to_string(&Registry::test()).unwrap();
                        let renewal =
                            toml_edit::ser::to_string(&Source::from((url, username))).unwrap();
                        assert_eq!(
                            fs::read_to_string(path).unwrap(),
                            format!(
                                r"[{REGISTRIES}.{REGISTRY}]
{registry}
[{NAME}.{REGISTRY}]
{renewal}"
                            )
                        )
                    }
                    Err(error) => err.assert_err(error),
                }
            }
        }
    }

    #[tokio::test]
    async fn generate() {
        struct Out {
            void: bool,
            content: Vec<u8>,
        }

        impl Out {
            fn error() -> io::Error {
                io::Error::other(
                    "And if thou gaze long into an abyss, the abyss will also gaze into thee.",
                )
            }
        }

        impl From<bool> for Out {
            fn from(void: bool) -> Self {
                Self {
                    void,
                    content: vec![],
                }
            }
        }

        impl Output for Out {
            fn content(&mut self) -> String {
                self.content.content()
            }
        }

        impl io::Write for Out {
            fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
                if self.void {
                    Err(Self::error())
                } else {
                    self.content.write(buf)
                }
            }

            fn flush(&mut self) -> io::Result<()> {
                self.content.flush()
            }
        }

        for subcommand in ["generate", "g"] {
            for (username, result, void) in [
                (USERNAME, Ok(Token::test()), false),
                (
                    Wanderer::NAME,
                    Err(anyhow!(source::Error::StatusCode(StatusCode::UNAUTHORIZED))),
                    false,
                ),
                (USERNAME, Err(anyhow!(Out::error())), true),
            ] {
                let artifactory = Artifactory::default();
                let mut out = Buffer::from(Out::from(void));
                let mut err = Buffer::err();

                run::<System, Hardcoded, _, _>(
                    Opts::try_parse_from([
                        NAME,
                        subcommand,
                        "-e",
                        artifactory.url().as_str(),
                        "-u",
                        username,
                        "-d",
                        &duration().to_string(),
                    ]),
                    Log::boring(&mut out, &mut err),
                )
                .await;

                match result {
                    Ok(token) => {
                        out.assert_out(token);
                        err.assert_empty()
                    }
                    Err(error) => {
                        out.assert_empty();
                        err.assert_err(error)
                    }
                }
            }
        }
    }

    #[tokio::test]
    async fn renew() {
        let artifactory = Artifactory::default();

        for subcommand in ["renew", "r"] {
            for (source, result) in [
                (None, Err(anyhow!(RenewError::Missing))),
                (Some(Source::from(artifactory.url())), Ok(())),
                (
                    Some(Source::from((artifactory.url(), Wanderer::NAME))),
                    Err(anyhow!(
                        source::Error::StatusCode(StatusCode::UNAUTHORIZED,)
                    )),
                ),
            ] {
                let file = NamedTempFile::new().unwrap();
                let path = file.path().display().to_string();
                let registry = toml_edit::ser::to_string(&Registry::test()).unwrap();

                let renewal = source.map(|source| {
                    let renewal = toml_edit::ser::to_string(&source).unwrap();
                    fs::write(
                        &path,
                        format!(
                            r"[{REGISTRIES}.{REGISTRY}]

[{NAME}.{REGISTRY}]
{renewal}"
                        ),
                    )
                    .unwrap();
                    renewal
                });

                let mut out = Buffer::out();
                let mut err = Buffer::err();

                run::<System, Hardcoded, _, _>(
                    Opts::try_parse_from([NAME, subcommand, "-f", &path, "-r", REGISTRY]),
                    Log::boring(&mut out, &mut err),
                )
                .await;

                out.assert_empty();

                match result {
                    Ok(()) => {
                        err.assert_empty();
                        let renewal = renewal.unwrap();
                        assert_eq!(
                            fs::read_to_string(&path).unwrap(),
                            format!(
                                r"[{REGISTRIES}.{REGISTRY}]
{registry}
[{NAME}.{REGISTRY}]
{renewal}"
                            )
                        );
                    }
                    Err(error) => err.assert_err(error),
                }
            }
        }
    }

    #[tokio::test]
    async fn save() {
        for subcommand in ["save", "s"] {
            for (file, result) in [
                (NamedTempFile::new().unwrap(), Ok(())),
                (
                    {
                        let file = NamedTempFile::new().unwrap();
                        readonly(&file);
                        file
                    },
                    Err(SaveError::Write(io::Error::last_os_error())),
                ),
            ] {
                let token = Token::test();

                let mut out = Buffer::out();
                let mut err = Buffer::err();

                run::<System, Hardcoded, _, _>(
                    Opts::try_parse_from([
                        NAME,
                        subcommand,
                        "-r",
                        REGISTRY,
                        "--access-token",
                        &token.access_token,
                        "--token-type",
                        &token.token_type,
                        "-f",
                        &file.path().display().to_string(),
                    ]),
                    Log::boring(&mut out, &mut err),
                )
                .await;

                out.assert_empty();

                match result {
                    Ok(()) => {
                        err.assert_empty();
                        let registry = toml_edit::ser::to_string(&Registry::test()).unwrap();

                        assert_eq!(
                            fs::read_to_string(file.path()).unwrap(),
                            format!(
                                r"[{REGISTRIES}.{REGISTRY}]
{registry}"
                            )
                        );
                    }
                    Err(error) => err.assert_err(error),
                }
            }
        }
    }

    #[test]
    fn no_home() {
        assert!(Opts::<Wanderer>::try_parse_from([NAME])
            .unwrap_err()
            .to_string()
            .starts_with(
                "error: the following required arguments were not provided:\n  --file <FILE>"
            ))
    }

    pub fn readonly(file: &NamedTempFile) {
        let mut permissions = fs::metadata(file).unwrap().permissions();
        permissions.set_readonly(true);
        fs::set_permissions(file, permissions).unwrap();

        #[cfg(unix)]
        if is_root() {
            file.path().set_flags(Flags::IMMUTABLE).unwrap()
        }
    }
}
