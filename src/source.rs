use std::io;

use clap::Args;
use reqwest::{Client, StatusCode};
use secrecy::ExposeSecret;
use serde::{Deserialize, Serialize};
use serde_with::{serde_as, DisplayFromStr};
use url::Url;

use crate::{
    artifactory,
    log::{progress, Log},
    password::Password,
    Token,
};

#[serde_as]
#[derive(Args, Deserialize, Serialize)]
#[cfg_attr(test, derive(Debug, PartialEq))]
pub struct Source {
    /// URL of the Artifactory token creation endpoint
    #[clap(
        short,
        long,
        env,
        default_value = artifactory::default()
    )]
    endpoint: Url,

    /// Your Artifactory username (e.g. `flastname` for First Lastname)
    #[clap(short, long, env, default_value_t = whoami::username())]
    username: String,

    /// Desired token duration
    #[clap(short, long, env)]
    #[serde_as(as = "Option<DisplayFromStr>")]
    duration: Option<humantime::Duration>,
}

impl Source {
    pub async fn token<P, Out, Err>(&self, log: &mut Log<Out, Err>) -> Result<Token, Error>
    where
        P: Password,
        Out: io::Write,
        Err: io::Write,
    {
        log.info(format!(
            "Request a new token with {} for <user>{}</> from <artifactory>{}</>",
            match self.duration {
                Some(duration) => format!("a duration of <duration>{duration}</>"),
                None => "<duration>default</> duration".to_string(),
            },
            self.username,
            self.endpoint
        ));

        let password = log.password::<P>()?;

        let request = artifactory::Request::from((self.username.clone(), self.duration));
        let response = progress!(log, "Send token request", async {
            Client::default()
                .post(self.endpoint.clone())
                .basic_auth(&self.username, Some(password.expose_secret()))
                .form(&request)
                .send()
                .await
        })?;

        let status = response.status();
        if status == StatusCode::OK || status == StatusCode::BAD_REQUEST {
            let token = Result::from(response.json::<artifactory::Response>().await?)?;
            log.newline();
            Ok(token)
        } else {
            Err(Error::StatusCode(status))
        }
    }
}

impl From<artifactory::Response> for Result<Token, Error> {
    fn from(value: artifactory::Response) -> Self {
        match value {
            artifactory::Response::Ok(token) => Ok(token),
            artifactory::Response::Err(error) => Err(error.into()),
        }
    }
}

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("Failed to <error>read password</>")]
    Password(#[from] io::Error),
    #[error("<artifactory>Artifactory</> replied with an <error>error message</>")]
    Artifactory(#[from] artifactory::Error),
    #[error(
        "<artifactory>Artifactory</> replied with an <warning>HTTP error code</>: <error>{0}</>"
    )]
    StatusCode(StatusCode),
    #[error("Failed to <error>read response</> from <artifactory>Artifactory</>")]
    Reqwest(#[from] reqwest::Error),
}

#[cfg(test)]
mod tests {
    use std::time::Duration;

    use secrecy::SecretString;

    use super::*;
    use crate::{
        artifactory::tests::{duration, nonexistent, Artifactory, MAX_EXPIRES_IN},
        home::tests::Wanderer,
        password::tests::Hardcoded,
        tests::USERNAME,
    };

    #[actix_web::test]
    async fn password_failure() {
        const ERROR: &str = "test error";

        struct Failure;
        impl Password for Failure {
            fn password() -> io::Result<SecretString> {
                Err(io::Error::other(ERROR))
            }
        }

        let artifactory = Artifactory::default();
        let source = Source::from(artifactory.url());

        assert!(matches!(
            source.token::<Failure, _, _>(Log::empty()).await,
            Err(Error::Password(error))
            if error.to_string() == ERROR
        ))
    }

    #[actix_web::test]
    async fn bad_address() {
        let url = nonexistent();
        let source = Source::from(url.clone());

        assert!(matches!(
            source.token::<Hardcoded, _, _>(Log::empty()).await,
            Err(Error::Reqwest(error))
            if error.to_string() == format!("error sending request for url ({url}): error trying to connect: tcp connect error: Connection refused (os error 111)")
        ))
    }

    #[actix_web::test]
    async fn not_found() {
        let artifactory = Artifactory::default();

        let mut source = Source::from(artifactory.url());
        source.endpoint.set_path("/nonexistent");

        assert!(matches!(
            source.token::<Hardcoded, _, _>(Log::empty()).await,
            Err(Error::StatusCode(code)) if code == StatusCode::NOT_FOUND
        ))
    }

    #[actix_web::test]
    async fn unauthorized() {
        let artifactory = Artifactory::default();
        let source = Source::from((artifactory.url(), Wanderer::NAME));

        assert!(matches!(
            source.token::<Hardcoded, _, _>(Log::empty()).await,
            Err(Error::StatusCode(code)) if code == StatusCode::UNAUTHORIZED
        ))
    }

    #[actix_web::test]
    async fn bad_request() {
        const DURATION: u64 = MAX_EXPIRES_IN * 2;

        let artifactory = Artifactory::default();

        let mut source = Source::from(artifactory.url());
        source.duration = Some(Duration::from_secs(DURATION).into());

        assert!(matches!(
            source.token::<Hardcoded, _, _>(Log::empty()).await,
            Err(Error::Artifactory(error))
            if error == artifactory::Error::expires_in(USERNAME, DURATION)
        ))
    }

    #[actix_web::test]
    async fn unexpected() {
        let artifactory = Artifactory::new(());
        let source = Source::from(artifactory.url());

        assert!(matches!(
            source.token::<Hardcoded, _, _>(Log::empty()).await,
            Err(Error::Reqwest(error))
            if error.to_string() == "error decoding response body: data did not match any variant of untagged enum Response"
        ))
    }

    #[actix_web::test]
    async fn success() {
        let artifactory = Artifactory::default();
        let mut source = Source::from(artifactory.url());
        let token = Token::test();

        assert_eq!(
            source.token::<Hardcoded, _, _>(Log::empty()).await.unwrap(),
            token
        );

        source.duration = None;
        assert_eq!(
            source.token::<Hardcoded, _, _>(Log::empty()).await.unwrap(),
            token
        )
    }

    impl From<Url> for Source {
        fn from(endpoint: Url) -> Self {
            Self::from((endpoint, USERNAME))
        }
    }

    impl From<(Url, &str)> for Source {
        fn from((endpoint, username): (Url, &str)) -> Self {
            Self {
                endpoint,
                username: username.to_string(),
                duration: Some(duration()),
            }
        }
    }
}
