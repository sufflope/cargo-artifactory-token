use std::io;

use rpassword::prompt_password;
use secrecy::SecretString;

pub const PROMPT: &str = "Enter your password";

pub trait Password {
    fn password() -> io::Result<SecretString>;
}

pub struct Prompt;

impl Password for Prompt {
    fn password() -> io::Result<SecretString> {
        Ok(prompt_password(format!("{PROMPT}: "))?.into())
    }
}

#[cfg(test)]
pub mod tests {
    use super::*;

    pub struct Hardcoded;

    impl Password for Hardcoded {
        fn password() -> io::Result<SecretString> {
            Ok("some-secret-password".to_string().into())
        }
    }
}
