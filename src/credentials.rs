use std::{any::type_name, io, marker::PhantomData, path::PathBuf, str::FromStr};

use clap::{Arg, ArgMatches, Args, Command, FromArgMatches};
use fs_err as fs;
#[cfg(test)]
use serde::Deserialize;
use serde::Serialize;
use toml_edit::{
    de::from_str, ser::to_document, value, DocumentMut, ImDocument, Item, Table, TomlError,
};

use crate::{
    home::Home,
    log::{progress, Log},
    source::Source,
    Token, NAME, REGISTRY,
};

pub const REGISTRIES: &str = "registries";

#[derive(Args)]
#[cfg_attr(test, derive(Debug, PartialEq))]
pub struct Credentials<Path>
where
    Path: Args,
{
    #[clap(flatten)]
    path: Path,

    /// Name of the registry in your credentials file
    #[clap(short, long, env, default_value = REGISTRY)]
    registry: String,
}

impl<Default> Credentials<File<Default>>
where
    Default: Home,
{
    pub fn validate(self) -> Result<Credentials<Validated>, PathError> {
        Ok(Credentials {
            path: Validated {
                file: self.path.0.ok_or(PathError)?,
            },
            registry: self.registry,
        })
    }
}

impl Credentials<Validated> {
    pub fn renewal<Out, Err>(&self, log: &mut Log<Out, Err>) -> Result<Source, RenewError>
    where
        Out: io::Write,
        Err: io::Write,
    {
        log.info(format!(
            "Read renewal information for <artifactory>{}</> from <user>{}</>",
            self.registry,
            self.path.file.display()
        ));

        let document: ImDocument<String> = self.parse(log)?;

        let renewal = progress!(log, "Read renewal information", {
            let content = document
                .get(NAME)
                .ok_or(RenewError::Missing)?
                .get(&self.registry)
                .ok_or(RenewError::Registry(self.registry.clone()))?
                .to_string();
            Ok::<_, RenewError>(from_str(&content)?)
        })?;

        log.newline();
        Ok(renewal)
    }

    pub fn save<Out, Err>(
        &self,
        log: &mut Log<Out, Err>,
        token: Token,
        source: Option<&Source>,
    ) -> Result<(), SaveError>
    where
        Out: io::Write,
        Err: io::Write,
    {
        log.info(format!(
            "Save <artifactory>{}</> token to <user>{}</>",
            self.registry,
            self.path.file.display()
        ));

        fn insert<T>(table: &mut Table, t: T)
        where
            T: Serialize,
        {
            for (keys, val) in to_document(&t)
                .unwrap_or_else(|error| {
                    panic!(
                        "{:#}",
                        anyhow::Error::new(error).context(format!(
                            "{} should always serialize as valid TOML",
                            type_name::<T>()
                        ))
                    )
                })
                .get_values()
            {
                for key in keys {
                    table[key] = value(val)
                }
            }
        }

        fn table<'t>(
            root: &'t mut Table,
            key: &str,
            error: impl Fn(String) -> SaveError,
        ) -> Result<&'t mut Table, SaveError> {
            root.entry(key)
                .or_insert_with(|| {
                    let mut table = Table::new();
                    table.set_implicit(true);
                    Item::Table(table)
                })
                .as_table_mut()
                .ok_or(error(key.to_string()))
        }

        let mut credentials: DocumentMut = match self.parse(log) {
            Ok(document) => Ok(document),
            Err(ParseError::Missing(_)) => {
                log.warn("<details>No existing credentials</>");
                Ok(DocumentMut::new())
            }
            Err(other) => Err(other),
        }?;

        progress!(log, "Add token to registry", {
            let registries = table(&mut credentials, REGISTRIES, SaveError::NotATable)?;
            let registry = table(
                registries,
                &self.registry,
                SaveError::not_a_table(REGISTRIES),
            )?;
            insert(registry, Registry::from(token));
            Ok::<_, SaveError>(())
        })?;

        if let Some(source) = source {
            progress!(log, "Add renewal information", {
                let renewal = table(&mut credentials, NAME, SaveError::NotATable)?;
                let registry = table(renewal, &self.registry, SaveError::not_a_table(NAME))?;
                insert(registry, source);
                Ok::<_, SaveError>(())
            })?;
        }

        progress!(
            log,
            "Save changes",
            fs::write(&self.path.file, credentials.to_string())
        )?;

        log.newline();
        Ok(())
    }

    fn parse<D, Out, Err>(&self, log: &mut Log<Out, Err>) -> Result<D, ParseError>
    where
        D: FromStr,
        ParseError: From<D::Err>,
        Out: io::Write,
        Err: io::Write,
    {
        progress!(
            log,
            "Load credentials",
            Ok::<_, ParseError>(fs::read_to_string(&self.path.file)?.parse::<D>()?)
        )
    }
}

#[cfg_attr(test, derive(Debug))]
pub struct File<Default>(Option<PathBuf>, PhantomData<Default>);

impl<Default> File<Default> {
    const ID: &'static str = "file";
}

impl<Default, T> From<Option<T>> for File<Default>
where
    T: Into<PathBuf>,
{
    fn from(value: Option<T>) -> Self {
        Self(value.map(Into::into), PhantomData)
    }
}

impl<Default> Args for File<Default>
where
    Default: Home,
{
    fn augment_args(cmd: Command) -> Command {
        let mut arg = Arg::new(Self::ID)
            .short(Self::ID.chars().nth(0))
            .long(Self::ID)
            .env(Self::ID.to_uppercase())
            .value_name(Self::ID.to_uppercase())
            .help("Path to your cargo credentials file");
        arg = match Default::home() {
            Some(mut path) => {
                path.push(".cargo");
                path.push("credentials.toml");
                arg.default_value(path.into_os_string())
            }
            _ => arg.required(true),
        };
        cmd.arg(arg)
    }

    fn augment_args_for_update(cmd: Command) -> Command {
        Self::augment_args(cmd)
    }
}

impl<Default> FromArgMatches for File<Default> {
    fn from_arg_matches(matches: &ArgMatches) -> Result<Self, clap::Error> {
        Ok(matches.get_one::<String>(Self::ID).into())
    }

    fn update_from_arg_matches(&mut self, matches: &ArgMatches) -> Result<(), clap::Error> {
        self.0 = Self::from_arg_matches(matches)?.0;
        Ok(())
    }
}

#[derive(Args)]
#[cfg_attr(test, derive(Debug, PartialEq))]
pub struct Validated {
    file: PathBuf,
}

#[derive(Serialize)]
#[cfg_attr(test, derive(Debug, Deserialize, PartialEq))]
pub struct Registry {
    token: String,
}

impl From<Token> for Registry {
    fn from(token: Token) -> Self {
        Self {
            token: token.to_string(),
        }
    }
}

#[derive(Debug, thiserror::Error)]
#[cfg_attr(test, derive(PartialEq))]
#[error("<error>No credentials path</> provided and <warning>could not determine</> <user>home directory</>")]
pub struct PathError;

#[derive(Debug, thiserror::Error)]
pub enum ParseError {
    #[error("Failed to <error>read credentials</>")]
    Missing(#[from] io::Error),
    #[error("Failed to <error>parse credentials</>")]
    Underlying(#[from] TomlError),
}

#[derive(Debug, thiserror::Error)]
pub enum RenewError {
    #[error("Failed to <error>parse renewal information</>")]
    Deserialize(#[from] toml_edit::de::Error),
    #[error(transparent)]
    Parse(#[from] ParseError),
    #[error("<error>No renewal information</>")]
    Missing,
    #[error("<error>No renewal information</> for registry <artifactory>{0}</>")]
    Registry(String),
}

#[derive(Debug, thiserror::Error)]
pub enum SaveError {
    #[error(transparent)]
    Parse(#[from] ParseError),
    #[error("Key <artifactory>{0}</> is <warning>already present</> in credentials but is <error>not a TOML table</>")]
    NotATable(String),
    #[error("Failed to <error>save credentials</>")]
    Write(#[from] io::Error),
}

impl SaveError {
    fn not_a_table(prefix: &str) -> impl Fn(String) -> Self + '_ {
        move |key| Self::NotATable(format!("{prefix}.{key}"))
    }
}

#[cfg(test)]
mod tests {
    use std::fmt;

    use tempfile::NamedTempFile;
    use toml_edit::ser::to_string;

    use super::*;
    use crate::{artifactory::tests::nonexistent, home::tests::Wanderer};

    #[test]
    fn no_path() {
        assert_eq!(
            Credentials::<File<Wanderer>> {
                path: Wanderer::home().into(),
                registry: REGISTRY.to_string()
            }
            .validate(),
            Err(PathError)
        )
    }

    mod renew {
        use super::*;

        #[test]
        fn file_does_not_exist() {
            let test = Test::empty();

            fs::remove_file(&test.file).unwrap();
            assert!(!test.file.path().exists());

            assert!(matches!(
                test.credentials.renewal(Log::empty()),
                Err(RenewError::Parse(ParseError::Missing(error)))
                if error.to_string() == format!("failed to open file `{}`", test.file.path().display())
            ))
        }

        #[test]
        fn renewal_key_does_not_exist() {
            let test = Test::empty();

            test.credentials
                .save(Log::empty(), Token::test(), None)
                .unwrap();

            assert!(matches!(
                test.credentials.renewal(Log::empty()),
                Err(RenewError::Missing)
            ))
        }

        #[test]
        fn registry_key_does_not_exist() {
            let test = Test::with_unknown_entry(&[NAME]);

            assert!(matches!(
                test.credentials.renewal(Log::empty()),
                Err(RenewError::Registry(registry))
                if registry == REGISTRY
            ))
        }

        #[test]
        fn registry_key_is_invalid() {
            let test = Test::with_unknown_entry(&[NAME, REGISTRY]);

            let entry = to_string(&Unknown::default()).unwrap().trim().to_string();
            let underline = "^".repeat(entry.len());
            let message = format!(
                r"TOML parse error at line 1, column 1
  |
1 | {entry}
  | {underline}
missing field `endpoint`
",
            );

            assert!(matches!(
                test.credentials.renewal(Log::empty()),
                Err(RenewError::Deserialize(error))
                if error.to_string() == message
            ))
        }

        #[test]
        fn renewal() {
            let test = Test::empty();
            let source = Source::from(nonexistent());

            test.credentials
                .save(Log::empty(), Token::test(), Some(&source))
                .unwrap();

            assert_eq!(test.credentials.renewal(Log::empty()).unwrap(), source)
        }
    }

    mod save {
        use super::*;
        use crate::tests::readonly;

        #[test]
        fn file_does_not_exist() {
            let test = Test::empty();

            fs::remove_file(&test.file).unwrap();
            assert!(!test.file.path().exists());

            test.credentials
                .save(Log::empty(), Token::test(), None)
                .unwrap();

            assert_eq!(test.parse(&[REGISTRIES, REGISTRY]), Ok(Registry::test()))
        }

        #[test]
        fn file_is_not_writable() {
            let test = Test::empty();

            readonly(&test.file);

            assert!(matches!(
                test.credentials.save(Log::empty(), Token::test(), None),
                Err(SaveError::Write(error))
                if error.to_string() == format!("failed to create file `{}`", test.file.path().display())
            ))
        }

        #[test]
        fn credentials_are_invalid_toml() {
            let xml = "<xml />";
            let test = Test::from(xml);
            let message = format!(
                r"TOML parse error at line 1, column 1
  |
1 | {xml}
  | ^
invalid key
"
            );

            assert!(matches!(
                test.credentials.save(Log::empty(), Token::test(), None),
                Err(SaveError::Parse(ParseError::Underlying(error)))
                if error.to_string() == message
            ))
        }

        #[test]
        fn registries_key_does_not_exist() {
            const CUSTOM: &str = "custom";

            let test = Test::with_unknown_entry(&[CUSTOM]);

            test.credentials
                .save(Log::empty(), Token::test(), None)
                .unwrap();

            assert_eq!(test.parse(&[REGISTRIES, REGISTRY]), Ok(Registry::test()));
            assert_eq!(test.parse(&[CUSTOM]), Ok(Unknown::default()));
        }

        #[test]
        fn registries_key_is_not_a_table() {
            let test = Test::with_not_a_table_entry(&[], REGISTRIES);

            assert!(matches!(
                test.credentials.save(Log::empty(), Token::test(), None),
                Err(SaveError::NotATable(key))
                if key == REGISTRIES
            ))
        }

        #[test]
        fn registry_key_does_not_exist() {
            let test = Test::with_unknown_entry(&[REGISTRIES]);

            test.credentials
                .save(Log::empty(), Token::test(), None)
                .unwrap();

            assert_eq!(test.parse(&[REGISTRIES, REGISTRY]), Ok(Registry::test()));
            assert_eq!(test.parse(&[REGISTRIES]), Ok(Unknown::default()))
        }

        #[test]
        fn registry_key_is_not_a_table() {
            let test = Test::with_not_a_table_entry(&[REGISTRIES], REGISTRY);

            assert!(matches!(
                test.credentials.save(Log::empty(),Token::test(), None),
                Err(SaveError::NotATable(key))
                if key == format!("{REGISTRIES}.{REGISTRY}")
            ))
        }

        #[test]
        fn registry_key_exists() {
            let test = Test::with_unknown_entry(&[REGISTRIES, REGISTRY]);

            test.credentials
                .save(Log::empty(), Token::test(), None)
                .unwrap();

            assert_eq!(test.parse(&[REGISTRIES, REGISTRY]), Ok(Registry::test()));
            assert_eq!(test.parse(&[REGISTRIES, REGISTRY]), Ok(Unknown::default()));
        }

        #[test]
        fn renewals_key_is_not_a_table() {
            let test = Test::with_not_a_table_entry(&[], NAME);

            assert!(matches!(
                test.credentials.save(Log::empty(), Token::test(), Some(&Source::from(nonexistent()))),
                Err(SaveError::NotATable(key))
                if key == NAME
            ));
        }

        #[test]
        fn renewal_key_is_not_a_table() {
            let test = Test::with_not_a_table_entry(&[NAME], REGISTRY);

            assert!(matches!(
                test.credentials.save(Log::empty(), Token::test(), Some(&Source::from(nonexistent()))),
                Err(SaveError::NotATable(key))
                if key == format!("{NAME}.{REGISTRY}")
            ))
        }
    }

    impl Registry {
        pub fn test() -> Self {
            Token::test().into()
        }
    }

    #[derive(Debug, Deserialize, PartialEq, Serialize)]
    struct Unknown {
        baz: String,
    }

    impl Default for Unknown {
        fn default() -> Self {
            Self {
                baz: "buz".to_string(),
            }
        }
    }

    struct Test {
        file: NamedTempFile,
        credentials: Credentials<Validated>,
    }

    impl Test {
        fn empty() -> Self {
            Self::from(None::<String>)
        }

        fn parse<T>(&self, path: &[&str]) -> Result<T, toml_edit::de::Error>
        where
            T: for<'de> Deserialize<'de>,
        {
            let document: DocumentMut = self.credentials.parse(Log::empty()).unwrap();

            let mut entry = document.as_item();
            for key in path {
                entry = &entry[key];
            }

            from_str(&entry.to_string())
        }

        fn with_entry<E>(path: &[&str], entry: E) -> Self
        where
            E: fmt::Display,
        {
            let key = if path.is_empty() {
                None
            } else {
                Some(path.join("."))
            }
            .map(|key| format!("[{key}]"))
            .unwrap_or_default();
            Self::from(
                format!(
                    r"{key}
{entry}
            "
                )
                .as_str(),
            )
        }

        fn with_not_a_table_entry(path: &[&str], key: &str) -> Self {
            Self::with_entry(path, format!("{key} = 0"))
        }

        fn with_unknown_entry(path: &[&str]) -> Self {
            Self::with_entry(path, to_string(&Unknown::default()).unwrap())
        }
    }

    impl From<&str> for Test {
        fn from(value: &str) -> Self {
            Self::from(Some(value))
        }
    }

    impl<C> From<Option<C>> for Test
    where
        C: AsRef<[u8]>,
    {
        fn from(value: Option<C>) -> Self {
            let file = NamedTempFile::new().unwrap();
            let path = file.path();
            if let Some(content) = value {
                fs::write(path, content).unwrap();
            }
            let credentials = Credentials {
                path: Validated {
                    file: file.path().to_path_buf(),
                },
                registry: REGISTRY.to_string(),
            };
            Self { file, credentials }
        }
    }
}
