use std::path::PathBuf;

use dirs::home_dir;

pub trait Home {
    fn home() -> Option<PathBuf>;
}

pub struct System;

impl Home for System {
    fn home() -> Option<PathBuf> {
        home_dir()
    }
}

#[cfg(test)]
pub mod tests {
    use super::*;

    #[derive(Debug)]
    pub struct Wanderer;

    impl Wanderer {
        pub const NAME: &'static str = "call me what you will";
    }

    impl Home for Wanderer {
        fn home() -> Option<PathBuf> {
            None
        }
    }
}
