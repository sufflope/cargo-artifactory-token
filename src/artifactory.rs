use addr::parse_domain_name;
use serde::{Deserialize, Serialize};
use whoami::fallible::hostname;

use crate::{Token, REGISTRY};

const LOCALHOST: &str = "localhost";
const PATH: &str = "access/api/v1/tokens";

pub fn default() -> String {
    let domain = hostname()
        .ok()
        .as_ref()
        .and_then(|hostname| parse_domain_name(hostname).ok())
        .and_then(|domain| domain.root())
        .map(|domain| format!("{REGISTRY}.{domain}"))
        .unwrap_or(LOCALHOST.to_string());
    format!("https://{domain}/{PATH}")
}

#[derive(Serialize)]
#[cfg_attr(test, derive(Deserialize))]
pub struct Request {
    username: String,
    expires_in: Option<u64>,
}

impl From<(String, Option<humantime::Duration>)> for Request {
    fn from((username, duration): (String, Option<humantime::Duration>)) -> Self {
        Self {
            username: username.to_string(),
            expires_in: duration.map(|duration| duration.as_secs()),
        }
    }
}

#[derive(Deserialize)]
#[cfg_attr(test, derive(Serialize))]
#[serde(untagged)]
pub enum Response {
    Ok(Token),
    Err(Error),
}

#[derive(Debug, Deserialize, thiserror::Error)]
#[cfg_attr(test, derive(PartialEq, Serialize))]
#[error("<error>{error}</>: <details>{error_description}</>")]
pub struct Error {
    error: String,
    error_description: String,
}

#[cfg(test)]
pub mod tests {
    use std::{
        fmt,
        future::{ready, Ready},
        io,
        sync::{Arc, OnceLock},
        time::Duration,
    };

    use actix_web::{
        web::{post, Form},
        App, Handler, HttpResponse, HttpServer,
    };
    use tokio::task::JoinHandle;
    use url::Url;

    use super::*;
    use crate::tests::USERNAME;

    pub const MAX_EXPIRES_IN: u64 = 3600;

    pub struct Artifactory {
        handle: JoinHandle<io::Result<()>>,
        port: u16,
    }

    struct Endpoint<T>(Arc<T>);

    impl<T> Clone for Endpoint<T> {
        fn clone(&self) -> Self {
            Self(self.0.clone())
        }
    }

    impl<T> Handler<Form<Request>> for Endpoint<T>
    where
        T: Serialize + 'static,
    {
        type Output = HttpResponse;
        type Future = Ready<HttpResponse>;

        fn call(&self, args: Form<Request>) -> Self::Future {
            let request = args.into_inner();

            ready(if request.username == USERNAME {
                match request.expires_in {
                    Some(value) if value > MAX_EXPIRES_IN => HttpResponse::Ok()
                        .json(Response::Err(Error::expires_in(request.username, value))),
                    _ => HttpResponse::Ok().json(self.0.as_ref()),
                }
            } else {
                HttpResponse::Unauthorized().finish()
            })
        }
    }

    impl Artifactory {
        pub fn new<T>(response: T) -> Self
        where
            T: Send + Serialize + Sync + 'static,
        {
            let response = Arc::new(response);
            let server = HttpServer::new(move || {
                App::new().route(
                    &format!("/{PATH}"),
                    post().to::<Endpoint<T>, Form<Request>>(Endpoint(response.clone())),
                )
            })
            .disable_signals()
            .bind(format!("{LOCALHOST}:0"))
            .unwrap();

            let port = server.addrs().first().unwrap().port();
            let handle = tokio::spawn(server.run());

            Self { handle, port }
        }

        pub fn url(&self) -> Url {
            Url::parse(&format!("http://{}:{}/{}", LOCALHOST, self.port, PATH)).unwrap()
        }
    }

    impl Default for Artifactory {
        fn default() -> Self {
            Self::new(Token::test())
        }
    }

    impl Drop for Artifactory {
        fn drop(&mut self) {
            self.handle.abort()
        }
    }

    impl Error {
        pub fn expires_in<U>(username: U, value: u64) -> Self
        where
            U: fmt::Display,
        {
            Self {
                error: "invalid_request".to_string(),
                error_description: format!("The user: '{username}' can only create user token with expires in larger than 0 and smaller than {MAX_EXPIRES_IN} seconds (requested: {value})"),
            }
        }
    }

    pub fn duration() -> humantime::Duration {
        Duration::from_secs(MAX_EXPIRES_IN / 2).into()
    }

    pub fn nonexistent() -> Url {
        static VALUE: OnceLock<Url> = OnceLock::new();
        VALUE
            .get_or_init(|| Url::parse(&format!("http://{LOCALHOST}")).unwrap())
            .clone()
    }
}
