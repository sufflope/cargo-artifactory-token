use std::{fmt::Display, io, sync::Mutex};

use paris::Logger;
use secrecy::SecretString;

use crate::password::{Password, PROMPT};

pub const LINE_UP: &str = "\x1b[A";

pub struct Log<Out, Err>(Variant<Out, Err>);

enum Variant<Out, Err> {
    Boring { out: Out, err: Err },
    Fancy(Logger<'static>),
}

impl<Out, Err> Log<Out, Err>
where
    Out: io::Write,
    Err: io::Write,
{
    pub fn boring(out: Out, err: Err) -> Self {
        Self(Variant::Boring { out, err })
    }

    fn fancy() -> Self {
        let mut logger = Logger::new();
        logger
            .add_style("artifactory", vec!["bold", "green"])
            .add_style("details", vec!["dimmed"])
            .add_style("duration", vec!["bold", "cyan"])
            .add_style("error", vec!["bold", "red"])
            .add_style("user", vec!["bold", "magenta"])
            .add_style("warning", vec!["bold", "yellow"]);
        Self(Variant::Fancy(logger))
    }

    pub fn log<M>(&mut self, message: M) -> &mut Self
    where
        M: Display,
    {
        match self {
            Self(Variant::Boring { .. }) => (),
            Self(Variant::Fancy(paris)) => {
                paris.log(message);
            }
        }
        self
    }

    pub fn success<M>(&mut self, message: M) -> &mut Self
    where
        M: Display,
    {
        match self {
            Self(Variant::Boring { .. }) => self.log(message),
            Self(Variant::Fancy(paris)) => {
                paris.success(message);
                self
            }
        }
    }

    pub fn info<M>(&mut self, message: M) -> &mut Self
    where
        M: Display,
    {
        match self {
            Self(Variant::Boring { .. }) => self.log(message),
            Self(Variant::Fancy(paris)) => {
                paris.info(message);
                self
            }
        }
    }

    pub fn warn<M>(&mut self, message: M) -> &mut Self
    where
        M: Display,
    {
        match self {
            Self(Variant::Boring { .. }) => self.log(message),
            Self(Variant::Fancy(paris)) => {
                paris.warn(message);
                self
            }
        }
    }

    pub fn error<M>(&mut self, message: M) -> &mut Self
    where
        M: Display,
    {
        match self {
            Self(Variant::Boring { .. }) => self.log(message),
            Self(Variant::Fancy(paris)) => {
                paris.error(message);
                self
            }
        }
    }

    pub fn loading<M>(&mut self, message: M) -> &mut Self
    where
        M: Display,
    {
        match self {
            Self(Variant::Boring { .. }) => self.log(message),
            Self(Variant::Fancy(paris)) => {
                paris.loading(message);
                self
            }
        }
    }

    pub fn newline(&mut self) -> &mut Self {
        match self {
            Self(Variant::Boring { .. }) => self.log('\n'),
            Self(Variant::Fancy(paris)) => {
                paris.newline(1);
                self
            }
        }
    }

    pub fn password<P>(&mut self) -> io::Result<SecretString>
    where
        P: Password,
    {
        match self {
            Self(Variant::Boring { .. }) => P::password(),
            Self(Variant::Fancy(_)) => progress!(self, PROMPT, {
                let password = P::password();
                eprint!("{}", LINE_UP);
                password
            }),
        }
    }

    pub fn out<M>(&mut self, message: M) -> io::Result<&mut Self>
    where
        M: Display,
    {
        match self {
            Self(Variant::Boring { out, .. }) => writeln!(out, "{}", clean(message))?,
            Self(Variant::Fancy(paris)) => {
                paris.log(message);
            }
        }
        Ok(self)
    }

    pub fn finish(mut self, result: anyhow::Result<()>) {
        match result {
            Ok(()) => {
                self.log("<red><heart></> <bold>Done!</>");
            }
            Err(error) => {
                if let Some(error) = error.downcast_ref::<clap::Error>() {
                    error.print().unwrap();
                } else {
                    match self {
                        Self(Variant::Boring { mut err, .. }) => {
                            writeln!(err, "{}", clean(error)).unwrap()
                        }
                        Self(Variant::Fancy(mut paris)) => {
                            paris.newline(1).error(error.to_string());

                            let mut source = error.source();
                            let mut indent = 0;

                            while let Some(error) = source {
                                eprint!("{}\u{21B3} ", " ".repeat(indent));
                                paris.error(format!("<details>{error}</>"));
                                source = error.source();
                                indent += 2;
                            }
                        }
                    }
                }
            }
        }
    }
}

impl From<bool> for Log<io::Stdout, io::Stderr> {
    fn from(quiet: bool) -> Self {
        static LOCK: Mutex<bool> = Mutex::new(false);
        let mut locked = LOCK.lock().unwrap();
        if quiet || *locked {
            Self::boring(io::stdout(), io::stderr())
        } else {
            *locked = true;
            Self::fancy()
        }
    }
}

fn clean<M>(message: M) -> String
where
    M: Display,
{
    paris::formatter::format_string(message.to_string(), false)
}

macro_rules! progress {
    ($log:ident, $message:expr, async $expression:tt) => {
        $crate::log::progress!($log, $message, wrapped async { $expression }.await)
    };
    ($log:ident, $message:expr, wrapped $expression:expr) => {{
        let message = $message;
        let done = format!("<details>{}</>", &message);

        $log.loading(format!("{message}<blink>…</>"));

        #[allow(clippy::redundant_closure_call)]
        let val = $expression;
        match val {
            Ok(t) => {
                $log.success(done);
                Ok(t)
            }
            Err(e) => {
                $log.log($crate::log::LINE_UP);
                $log.error(done);
                Err(e)
            }
        }
    }};
    ($log:ident, $message:expr, $expression:expr) => {
        $crate::log::progress!($log, $message, wrapped (|| { $expression })())
    };
}

pub(crate) use progress;

#[cfg(test)]
pub mod tests {
    use std::mem;

    use super::*;

    impl Log<io::Empty, io::Empty> {
        pub fn empty() -> &'static mut Self {
            Box::leak(Box::new(Self::boring(io::empty(), io::empty())))
        }
    }

    pub trait Output: io::Write {
        fn content(&mut self) -> String;
    }

    impl Output for Vec<u8> {
        fn content(&mut self) -> String {
            String::from_utf8(mem::take(self)).unwrap()
        }
    }

    pub struct Buffer<B, const ERRORS: bool>(B);

    impl<B, const ERRORS: bool> Buffer<B, ERRORS>
    where
        B: Output,
    {
        fn assert<C>(&mut self, content: C)
        where
            C: AsRef<str>,
        {
            io::Write::flush(self).unwrap();
            assert_eq!(self.0.content(), content.as_ref())
        }

        pub fn assert_empty(&mut self) {
            self.assert("");
        }
    }

    impl<B> Buffer<B, false>
    where
        B: Output,
    {
        pub fn assert_out<C>(&mut self, content: C)
        where
            C: Display,
        {
            self.assert(format!("{}\n", content))
        }
    }

    impl Buffer<Vec<u8>, false> {
        pub fn out() -> Self {
            Self::from(vec![])
        }
    }

    impl<B> Buffer<B, true>
    where
        B: Output,
    {
        pub fn assert_err<E>(&mut self, error: E)
        where
            E: Into<anyhow::Error>,
        {
            self.assert(format!("{}\n", clean(error.into().to_string())));
        }
    }

    impl Buffer<Vec<u8>, true> {
        pub fn err() -> Self {
            Self::from(vec![])
        }
    }

    impl<B, const ERRORS: bool> From<B> for Buffer<B, ERRORS> {
        fn from(value: B) -> Self {
            Self(value)
        }
    }

    impl<B, const ERRORS: bool> io::Write for Buffer<B, ERRORS>
    where
        B: io::Write,
    {
        fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
            self.0.write(buf)
        }

        fn flush(&mut self) -> io::Result<()> {
            self.0.flush()
        }
    }

    #[test]
    fn fancy_is_unique() {
        assert!(matches!(Log::from(true), Log(Variant::Boring { .. })));
        assert!(matches!(Log::from(false), Log(Variant::Fancy(_))));
        assert!(matches!(Log::from(false), Log(Variant::Boring { .. })))
    }
}
