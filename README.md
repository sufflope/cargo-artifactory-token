# cargo-artifactory-token

[![License](https://img.shields.io/gitlab/license/sufflope%2Fcargo-artifactory-token)](https://spdx.org/licenses/GPL-3.0-only.html)
[![GitLab Release](https://img.shields.io/gitlab/v/release/sufflope%2Fcargo-artifactory-token)](https://gitlab.com/sufflope/cargo-artifactory-token/-/releases/permalink/latest)
[![Crates.io Version](https://img.shields.io/crates/v/cargo-artifactory-token)](https://crates.io/crates/cargo-artifactory-token)
[![Source Code Repository](https://img.shields.io/badge/Code-on%20GitLab-blue?logo=gitlab)](https://gitlab.com/sufflope/cargo-artifactory-token)
[![GitLab Pipeline Status](https://img.shields.io/gitlab/pipeline-status/sufflope%2Fcargo-artifactory-token)](https://gitlab.com/sufflope/cargo-artifactory-token/-/pipelines/)
[![GitLab Code Coverage](https://img.shields.io/gitlab/pipeline-coverage/sufflope%2Fcargo-artifactory-token)](https://gitlab.com/sufflope/cargo-artifactory-token/-/pipelines/)
[![Crates.io MSRV](https://img.shields.io/crates/msrv/cargo-artifactory-token)](https://releases.rs/docs/1.74.1/)

🦀-🐸-🪙 — A cargo subcommand to manage Artifactory access tokens.

![Screen recording of a token creation][__link0]{width=865}


## Installation


```shell
cargo install --locked cargo-artifactory-token
```


## Usage

See help for a detailed description of available commands, their arguments, and their default values:


```shell
cargo artifactory-token --help
```


### Arguments

Each argument `some-argument` can be set with a short (`-s`) or long (`--some-argument`) CLI argument, or read from an upper snake case environment variable (`SOME_ARGUMENT`).


#### Source

 - **endpoint**: URL of the Artifactory token creation endpoint
 - **username**: your Artifactory username (e.g. `flastname` for First Lastname)
 - **duration**: desired token duration


#### Credentials

 - **file**: path to your cargo credentials file
 - **registry**: name of the registry in your credentials file


#### Token

 - **access-token**: your private token value
 - **token-type**: your private token type


#### Global arguments

These arguments apply to all commands.

 - **quiet**: suppress all superfluous output


### Commands


#### Create a token

Retrieve a token and save it with its renewal information. This is the default subcommand.

Takes [Source](#source) and [Credentials](#credentials) arguments.


```shell
cargo artifactory-token create
```


#### Generate a token

Retrieve a token and display it.

Takes [Source](#source) arguments.


```shell
cargo artifactory-token generate
```


#### Renew a token

Retrieve a token using saved renewal information and save it.

Takes [Credentials](#credentials) arguments.


```shell
cargo artifactory-token renew
```


#### Save an existing token

Save a token retrieved by another means.

Takes [Credentials](#credentials) and [Token](#token) arguments.


```shell
cargo artifactory-token save
```



 [__link0]: https://gitlab.com/sufflope/cargo-artifactory-token/-/raw/main/docs/recording.webm
